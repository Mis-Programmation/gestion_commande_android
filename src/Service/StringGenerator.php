<?php

declare(strict_types=1);

namespace App\Service;

/**
 * Class StringGenerator
 *
 * @package App\Domain\Utils
 */
class StringGenerator
{
    const STRING = "QWERTYUIOPASDFGHJKLZXCVBNM123456789";

    /**
     * @param  int $length
     * @return string
     */
    public function generate(int $length = 4): string
    {
        return substr(str_repeat(str_shuffle(self::STRING), 10), 0, $length);
    }
}
