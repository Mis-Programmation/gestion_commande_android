<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DishRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"write:create"}},
 *     itemOperations={
 *          "GET"={},
 *          "PUT"={},
 *          "DELETE"={},
 *     }
 *
 * )
 *
 * @ORM\Entity(repositoryClass=DishRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Dish
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Assert\NotBlank()
     *  @Groups({"write:create"})
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     *  @Groups({"write:create"})
     *  @Assert\Url()
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $image = null;

    /**
     * @Assert\NotBlank()
     * @Groups({"write:create"})
     * @ORM\Column(type="text")
     */
    private string $descriptions;

    /**
     * @Assert\NotBlank()
     * @Assert\Positive()
     *  @Groups({"write:create"})
     * @ORM\Column(type="float")
     */
    private float $price;

    /**
     * @ORM\ManyToMany(targetEntity=Order::class, mappedBy="dish",fetch="EXTRA_LAZY")
     */
    private Collection $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescriptions(): ?string
    {
        return $this->descriptions;
    }

    public function setDescriptions(string $descriptions): self
    {
        $this->descriptions = $descriptions;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->addDish($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            $order->removeDish($this);
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function PrePersist(): void
    {
        if(null ===  $this->image)
        {
            $this->image = "https://via.placeholder.com/150";
        }
    }


}
