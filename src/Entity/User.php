<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"write:create"}},
 *     normalizationContext={"groups"={"read:user"}},
 *     collectionOperations={
            "POST"={},
 *     },
 *     itemOperations={
            "GET"={}
 *     }
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity(fields={"email","phone_number"})
 */
class User implements UserInterface,\Stringable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank(message="le numero est obligatoire")
     * @Groups({"write:create","read:user"})
     */
    public ?string $phone_number = null;

    /**
     * @Groups({"write:create","read:user"})
     * @Assert\Email()
     * @Assert\NotBlank()
     * @ORM\Column(type="string",nullable=false)
     */
    private ?string $email = null;

    /**
     *  @Groups({"read:user"})
     * @ORM\Column(type="string", length=255)
     */
    private ?string $password = null;

    /**
     * @Groups({"write:create"})
     * @Assert\NotBlank()
     * @Assert\Length(min="8", max=4096, minMessage="le mot de passe est trop court",maxMessage="le mot de passe est trop long")
     */
    public ?string $plain_password = null;

    /**
     * @Groups({"read:user"})
     * @ORM\Column(type="json")
     */
    private array $role = [];

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="users")
     */
    private $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): ?array
    {
        return $this->role;
    }

    /**
     * @return mixed
     */
    public function getEmail():?string
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function setRole(array $role): self
    {
        $this->role = $role;

        return $this;
    }


    public function getRoles():array
    {
        return $this->role;
    }

    public function getSalt():void
    {
    }

    public function getUsername():?string
    {
        return $this->email;
    }

    public function eraseCredentials():void
    {
       $this->plain_password = null;
    }

    public function __toString():string
    {
        return "ok";
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setUsers($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getUsers() === $this) {
                $order->setUsers(null);
            }
        }

        return $this;
    }

}
