<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }


    /**
     * @return int|mixed|string
     */
    public function getTotalOrderPrice():int
    {
        return  $this->createQueryBuilder('o')
            ->select("SUM(dish.price) as totalOrderPrice")
            ->leftJoin("o.dish",'dish')
            ->where("o.isAccepted = :bool")
            ->setParameter("bool",true)
            ->getQuery()->getResult()[0]['totalOrderPrice'];

    }

    /**
     * Permet de recuperper toutes les commandes valider
     * @return int
     */
    public function getTotalOrderValidate():int
    {
        return $this->createQueryBuilder('o')
            ->select("COUNT(o.id) as TotalOrderValidate")
            ->where("o.isAccepted = :bool")
            ->setParameter("bool",true)
            ->getQuery()->getResult()[0]['TotalOrderValidate'];
    }

    /**
     * permet de recuperper toutes les commades annuler
     * @return int
     */
    public function getTotalOrderNotValidate():int
    {
        return $this->createQueryBuilder('o')
            ->select("COUNT(o.id) as TotalOrderNotValidate")
            ->where("o.isAccepted = :bool")
            ->setParameter("bool",false)
            ->getQuery()->getResult()[0]['TotalOrderNotValidate'];
    }

    /**
     * Recuperer tous pas encore valide
     * @return array
     */
    public function getTotalRecentOrder():int
    {
        return $this->createQueryBuilder('o')
            ->select("COUNT(o.id) as TotalRecentOrder")
            ->where("o.isAccepted IS NULL")
            ->getQuery()->getResult()[0]['TotalRecentOrder'];
    }


}
