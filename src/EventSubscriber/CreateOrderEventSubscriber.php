<?php

declare(strict_types=1);

namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Order;
use App\Entity\User;
use App\Service\StringGenerator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class CreateOrderEventSubscriber implements EventSubscriberInterface
{

    private Security $security;
    /**
     * @var StringGenerator
     */
    private StringGenerator $generator;


    public function __construct(Security $security,StringGenerator $generator)
    {
        $this->security = $security;
        $this->generator = $generator;

    }

    public static function getSubscribedEvents():array
    {
        return [
            KernelEvents::VIEW => ['onCreate', EventPriorities::PRE_WRITE]
        ];
    }

    public function onCreate(ViewEvent $event):void
    {
        $method = $event->getRequest()->getMethod();
        $order  = $event->getControllerResult();

        if($order instanceof Order && $method === Request::METHOD_POST)
        {

            /** @var User $user */
            $user = $this->security->getUser();
            if(null === $user ) throw new \RuntimeException("Utilisateur n'est pas connecter");

            $order->setCreateAt(new \DateTimeImmutable('now'));
            $order->setUsers($user);
            $order->isAccepted(null);
            $order->setCode($this->generator->generate(5));

        }
    }
}
