<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

/**
 * Class JWTCustomEvent
 * @package App\EventSubscriber
 */
class JWTCustomEvent
{

    public function __invoke(JWTCreatedEvent $event):void
    {
        $data = $event->getData();
        /** @var User $user */
        $user = $event->getUser();

        $data['phone_number'] = $user->getPhoneNumber();
        $data['isAdmin'] = in_array("ROLE_ADMIN",$user->getRoles());

        $event->setData($data);

    }
}
