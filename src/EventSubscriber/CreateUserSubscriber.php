<?php

declare(strict_types=1);

namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class CreateUserSubscriber
 * @package App\EventSubscriber
 */
class CreateUserSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {

        $this->encoder = $encoder;
    }

    public static function getSubscribedEvents():array
    {
        return [
            KernelEvents::VIEW =>  ['onCreateUser', EventPriorities::PRE_WRITE]
        ];
    }

    public function onCreateUser(ViewEvent $event):void
    {
        /** @var User $user */
        $user = $event->getControllerResult();

        if($user instanceof UserInterface AND $event->getRequest()->getMethod() === Request::METHOD_POST)
        {

            $user->setPassword($this->encoder->encodePassword($user,$user->plain_password));
            $user->setRole([
                "ROLE_USER"
            ]);
        }

    }
}
