<?php


namespace App\Controller;


use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class DashboardControllerAPI extends AbstractController
{


    /**
     * @Route("/api/admin/index",name="admin_index",methods={"GET"})
     * @param OrderRepository $repository
     * @return JsonResponse
     * @throws \Exception
     */
    public function index(OrderRepository $repository):JsonResponse
    {


            if( !in_array("ROLE_USER",$this->getUser()->getRoles()))
            {
                return $this->json([
                    'message' => "Vous n'avez droid a cette pas",
                    "code" => Response::HTTP_FORBIDDEN
                ],Response::HTTP_FORBIDDEN);
            }

            $data = [
                'getTotalRecentOrder' => $repository->getTotalRecentOrder(),
                'getTotalOrderNotValidate' => $repository->getTotalOrderNotValidate(),
                'getTotalOrderValidate' => $repository->getTotalOrderValidate(),
                'getTotalOrderPrice' => $repository->getTotalOrderPrice(),
            ];
            return $this->json($data);



    }

}
